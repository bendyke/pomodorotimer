 
 
 //settings boxes
//pomodoro settings 
var pomodoroSettings = {
    pomodoroTimeText : document.querySelector("#pomodoro-time"),
    pomodoroTime : 25
}
setTimeText(pomodoroSettings.pomodoroTime, pomodoroSettings.pomodoroTimeText);
document.getElementById("pomodoro-minus").addEventListener("click", pomodoroMinusClick);
document.getElementById("pomodoro-plus").addEventListener("click", pomodoroPlusClick);

//Short break setings
var shortBreakSettings = {
    shortBreakTimeText : document.querySelector("#short-time"),
    shortTime : 5
}
setTimeText(shortBreakSettings.shortTime, shortBreakSettings.shortBreakTimeText);
document.getElementById("short-minus").addEventListener("click", shortMinusClick);
document.getElementById("short-plus").addEventListener("click", shortPlusClick);

//Long break settings
var longBreakSettings = {
    longBreakTimeText : document.querySelector("#long-time"),
    longTime : 25
}
setTimeText(longBreakSettings.longTime, longBreakSettings.longBreakTimeText);
document.getElementById("long-minus").addEventListener("click", longMinusClick);
document.getElementById("long-plus").addEventListener("click", longPlusClick);

//Timer elements into object
var timer = {
    timerText : document.querySelector(".timer-text"),
    timerTime : document.querySelector(".timer-time"),
    progressBar : document.querySelector(".progress-circle"),
    circumference : document.querySelector(".progress-circle").getTotalLength(),
    startButton : document.querySelector("#start-btn"),
    resetButton : document.querySelector("#reset-btn"),
    isActive : false,
    isPaused : false,
    pomodoroCount : 1,
    breakCount : 0,
    timerInterval: null,
    timerSegmentType: null,
    timerBeep : new Audio('beep.mp3'),
}
timer.startButton.addEventListener("click", startClicked);
timer.resetButton.addEventListener("click", resetClicked);


//set default timer values based on pomodoro settings
timer.timerText.innerHTML = "Pomodoro " + timer.pomodoroCount;
setTimeText(pomodoroSettings.pomodoroTime,timer.timerTime);




function startClicked () {
    //start timer with 1st pomodoro session by default
    if (!timer.isActive) {
        timer.isActive = true;
        timer.timerSegmentType = "pomodoro";
        startTimer(pomodoroSettings.pomodoroTime, timer.timerTime);
        timer.startButton.innerHTML = 'Pause';
        return;
    }
    //pause <-> unpause timer
    if (!timer.isPaused) {
        timer.isPaused = true;
        timer.startButton.innerHTML = "Start";
    }
    else {
        timer.isPaused = false;
        timer.startButton.innerHTML = 'Pause';
    }
} 

function resetClicked () {
    if (timer.timerInterval) {
         //clear + set interval to null -> Nincs jobb megoldas?
        clearInterval(timer.timerInterval);   
        timer.timerInterval = null;
        //reset boolean locks & pomodoro counter
        timer.isActive = false;
        timer.isPaused = false;
        timer.pomodoroCount = 1;
        //reset timer elements
        setTimeText(pomodoroSettings.pomodoroTime, timer.timerTime);
        timer.progressBar.style.strokeDashoffset = 0;
        timer.timerText.innerHTML = "Pomodoro " + timer.pomodoroCount;
        timer.startButton.innerHTML = "Start";
    }
}




function pomodoroMinusClick (){
    if (!timer.isActive && !timer.isPaused && pomodoroSettings.pomodoroTime > 1) {
        pomodoroSettings.pomodoroTime--;
        //Max 1 hour per session...  >1 hour per session needs extra formatting
        setTimeText(pomodoroSettings.pomodoroTime, pomodoroSettings.pomodoroTimeText);
        //update timer clock as well
        setTimeText(pomodoroSettings.pomodoroTime, timer.timerTime);
    }
}

function pomodoroPlusClick (){
    if (!timer.isActive && !timer.isPaused && pomodoroSettings.pomodoroTime < 60) {
        pomodoroSettings.pomodoroTime++;
        setTimeText(pomodoroSettings.pomodoroTime, pomodoroSettings.pomodoroTimeText);
        //update timer clock as well
        setTimeText(pomodoroSettings.pomodoroTime, timer.timerTime);
    }
}

function shortMinusClick () {
    if (!timer.isActive && !timer.isPaused && shortBreakSettings.shortTime > 0) {
        shortBreakSettings.shortTime--;
        setTimeText(shortBreakSettings.shortTime, shortBreakSettings.shortBreakTimeText);
    }
}

function shortPlusClick () {
    if (!timer.isActive && !timer.isPaused && shortBreakSettings.shortTime < 60) {
        shortBreakSettings.shortTime++;
        setTimeText(shortBreakSettings.shortTime, shortBreakSettings.shortBreakTimeText);
    }
}

function longMinusClick () {
    if (!timer.isActive && !timer.isPaused && longBreakSettings.longTime > 0) {
        longBreakSettings.longTime--;
        setTimeText(longBreakSettings.longTime, longBreakSettings.longBreakTimeText);
    }
}

function longPlusClick () {
    if (!timer.isActive && !timer.isPaused && longBreakSettings.longTime < 60) {
        longBreakSettings.longTime++;
        setTimeText(longBreakSettings.longTime, longBreakSettings.longBreakTimeText);
    }
}


function startTimer(duration, display) {
    //converting minute value to seconds for interval
    duration = duration* 60;
    //local vars for timer time, minutes and seconds
    let timerTime = duration, minutes, seconds;
    timer.timerInterval = setInterval(function () {

        if (!timer.isPaused) {
            //calculate minutes and seconds from current timer value
            minutes = parseInt(timerTime / 60, 10);
            seconds = parseInt(timerTime % 60, 10);

            //keep formatting
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.innerHTML = minutes + ":" + seconds;

            timer.progressBar.style.strokeDashoffset = 
                (timer.circumference / duration) * timerTime;

                timerTime--;
            if (timerTime < 0) {
                timer.timerBeep.play();
                clearInterval(timer.timerInterval);
                nextTimeInterval();
            }
        }
    }, 1000);
}

//generic function to keep 0 before single digit times
function setTimeText (timeVar, displayVar){
    displayVar.innerHTML = timeVar < 10 ? "0" + timeVar + ":00" : timeVar + ":00";
}

function nextTimeInterval(){
    //alternate based on active timeSegmentType
    //boolean-el nem olyan elegans szerintem ... ?
    if (timer.timerSegmentType == "pomodoro") {
        timer.breakCount++;
        if (timer.breakCount % 4 == 0) {
            startTimer(longBreakSettings.longTime, timer.timerTime);
            timer.timerSegmentType = "break";
            timer.timerText.innerHTML = "Long Break";
        }else {
            startTimer(shortBreakSettings.shortTime, timer.timerTime);
            timer.timerSegmentType = "break";
            timer.timerText.innerHTML = "Short Break";
        }
        return;
    }
    if (timer.timerSegmentType == "break") {
        timer.pomodoroCount++;
        startTimer(pomodoroSettings.pomodoroTime, timer.timerTime);
        timer.timerSegmentType = "pomodoro";
        timer.timerText.innerHTML = "Pomodoro " + timer.pomodoroCount;
    }
}
